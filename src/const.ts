import axios from "axios";

export const getCreatives = async (page: number|null = 0, limit: number|null = 10) => {
  return axios.get(`http://localhost:3001/creatives?_page=${page}&_limit=${limit}`)
}

export const getTotalCreatives = async () => {
  return axios.get(`http://localhost:3001/creatives`)
}

export const getCreativeByUuid = async (uuid: string) => {
  return axios.get(`http://localhost:3001/creatives/${uuid}`)
}

export const updateCreative = (uuid: string, data: any) => {
  return axios.put(`http://localhost:3001/creatives/${uuid}`, data)
}
