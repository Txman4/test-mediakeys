import React from "react";
import {Avatar, Chip, Grid, List, ListItem, ListItemText, Paper, Switch, Typography} from "@mui/material";
import {Contributors, CreativeProps, Formats} from "../types";
import {useQuery} from "react-query";
import {getCreatives} from "../const";
import Tooltip from "@mui/material/Tooltip";

interface Props {
  page: number;
  handleCreativeUuid: (uuid: string) => void;
}

export const Table = ({page, handleCreativeUuid}: Props) => {

  const {data} = useQuery(
    ['getCreatives', page],
    () => getCreatives(page),
  )
  const creatives = data?.data || []

  return <Grid item xs={8}>
    <Paper style={{padding: 16}} elevation={8}>
      <List>
        {creatives && creatives.map((creative: CreativeProps, index: number) => {
          return <ListItem
            key={`${creative.id}`}
            id={`${creative.id}`}
            secondaryAction={<Switch checked={creative.enabled}/>}
            divider={index < creatives.length - 1}
            onClick={() => handleCreativeUuid(creative.id)}
          >
            <ListItemText
              style={{cursor: "pointer"}}
              className={'listItemText'}
              primary={
                <Grid container spacing={1}>
                  <Grid item xs={3}>
                    <Typography variant="h6">
                      {creative.title}
                    </Typography>
                  </Grid>
                  <Grid item xs={3}>
                    <div style={{display: "flex"}}>
                      {creative.contributors && creative.contributors.map((user: Contributors) => (
                        <Tooltip key={`${creative.id}-${user.firstName}`} title={`${user.firstName} ${user.lastName}`}>
                          <Avatar style={{marginLeft: -16}}>
                            {user.firstName.charAt(1).toUpperCase()}
                            {user.lastName.charAt(1).toUpperCase()}
                          </Avatar>
                        </Tooltip>
                      ))}
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    {creative.formats && creative.formats.map((format: Formats) => (
                      <Chip
                        style={{marginRight: 8}}
                        key={`chip-table-${creative.id}-${format.width}x${format.height}`}
                        label={`${format.width}x${format.height}`}
                      />
                    ))}
                  </Grid>
                </Grid>
              }
            />
          </ListItem>
        })}
      </List>
    </Paper>
  </Grid>
}
