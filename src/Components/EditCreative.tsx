import React, {useEffect} from "react";

import {useMutation, useQuery, useQueryClient} from "react-query";
import {getCreativeByUuid, updateCreative} from "../const";

import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import {Add} from "@mui/icons-material";
import FormControlLabel from "@mui/material/FormControlLabel";
import {Button, Chip, Grid, IconButton, Switch, TextField} from "@mui/material";

import {modalStyle} from "../style";
import {CreativeProps} from "../types";


interface Props {
  uuid: string;
  open: boolean;
  handleClose: any;
}

interface FormatProps {
  width: number;
  height: number
}

export const EditCreative = ({uuid, open, handleClose}: Props) => {
  const [editedCreative, setEditedCreative] = React.useState<any>({});
  const [showFormatInputs, setShowFormatInputs] = React.useState<boolean>(false);
  const [addFormat, setAddFormat] = React.useState<FormatProps>({width: 0, height: 0});
  const [disableAddFormat, setDisableAddFormat] = React.useState<boolean>(true);

  const handleFormatDelete = (index: number) => {

    setEditedCreative((prevState: FormatProps) => ({
      ...prevState,
      'formats': editedCreative.formats.filter((item: FormatProps, i: number) => i !== index)
    }));
  };

  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    mutate()
    setAddFormat({width: 0, height: 0});
    handleClose(true);
  }

  const handleFormatChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setAddFormat((prevState: FormatProps) => ({
      ...prevState,
      [event.target.id]: Number(event.target.value)
    }));
    if (addFormat?.height > 0 && addFormat?.width > 0) {
      setDisableAddFormat(false)
    }
    if (event.target.value === '') {
      setDisableAddFormat(true)
    }
  }

  const handleChange = (event?: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, isSwitch?: boolean, isFormat?: boolean) => {
    if (isFormat && typeof addFormat !== 'boolean') {
      setEditedCreative((prevState: CreativeProps) => ({
        ...prevState,
        'formats': [...editedCreative.formats, addFormat]
      }));
      setAddFormat({width: 0, height: 0});
      setDisableAddFormat(false)
    } else {
      const target = event?.target as HTMLInputElement;
      setEditedCreative((prevState: CreativeProps) => ({
        ...prevState,
        [target.id]: isSwitch ? target.checked : event?.target.value,
      }));
    }
  };

  const queryClient = useQueryClient()

  const {data} = useQuery(['getCreativeByUuid', uuid], () => getCreativeByUuid(uuid))

  const {mutate} = useMutation(['getCreativeByUuid', uuid], () => updateCreative(uuid, editedCreative), {
    onSuccess: () => {
      queryClient.invalidateQueries('getCreatives')
      queryClient.invalidateQueries(['getCreativeByUuid', uuid])
    }
  })

  const creative = data?.data || []

  useEffect(() => {
    Object.keys(editedCreative).length === 0 && data?.data && setEditedCreative(data.data)
    data?.data?.id !== editedCreative.id && data?.data && setEditedCreative(data.data)
  }, [data, editedCreative])

  return <Modal
    open={open}
    onClose={handleClose}
    aria-labelledby="modal-modal-title"
    aria-describedby="modal-modal-description"
  >
    <Box sx={modalStyle}>
      <form onSubmit={onSubmit}>
        <Grid container alignItems="center">
          <Grid item xs={8}>
            <TextField
              id={'title'}
              margin="normal"
              label="Titre"
              defaultValue={editedCreative.title}
              onChange={(e) => handleChange(e)}
            />
          </Grid>
          <Grid item xs container justifyContent="flex-end">
            <Grid item>
              <FormControlLabel
                control={
                  <Switch id={'enabled'} onChange={(e) => handleChange(e, true)} checked={editedCreative.enabled}/>
                }
                label={''}/>
            </Grid>
          </Grid>
        </Grid>
        <TextField
          id={'description'}
          margin="normal"
          fullWidth
          multiline
          minRows={3}
          label="Description"
          defaultValue={editedCreative.description}
          onChange={(e) => handleChange(e)}
        />
        <TextField
          id={'content'}
          margin="normal"
          fullWidth
          multiline
          minRows={10}
          label="Contenu"
          defaultValue={editedCreative.content}
          onChange={(e) => handleChange(e)}
        />
        <Grid container spacing={2} alignItems="center">

          {editedCreative.formats ? editedCreative.formats?.map((format: { width: number, height: number }, index: number) => (
            <Grid item key={`chip-creative-${creative.id}-${format.width}x${format.height}`}>
              <Chip id={`chip-creative-${creative.id}-${format.width}x${format.height}`} onDelete={() => handleFormatDelete(index)} label={`${format.width}x${format.height}`}
                    color="primary"/>
            </Grid>
          )) : creative.formats?.map((format: { width: number, height: number }) => (
            <Grid item key={`chip-creative-${creative.id}-${format.width}x${format.height}`}>
              <Chip label={`${format.width}x${format.height}`} color="primary"/>
            </Grid>
          ))}

          <Grid item xs={12}>

            {!showFormatInputs ? <IconButton size="small" color="primary" onClick={() => setShowFormatInputs(true)}>
                <Add/>
              </IconButton>
              : <>
                <TextField
                  id={'width'}
                  type={'number'}
                  margin="normal"
                  fullWidth
                  label="Largeur"
                  onChange={(e) => handleFormatChange(e)}
                />
                <TextField
                  id={'height'}
                  type={'number'}
                  margin="normal"
                  fullWidth
                  label="Hauteur"
                  onChange={(e) => handleFormatChange(e)}
                />
                <Button disabled={disableAddFormat} color="primary" variant="contained"
                        onClick={() => handleChange( undefined,false, true)}>
                  Ajouter
                </Button>
              </>
            }

          </Grid>
        </Grid>
        <Grid item xs={3}/>
        <Grid item xs={3}/>
        <Grid item xs={12} container spacing={3} justifyContent="center" sx={{paddingTop: 2}}>
          <Grid item>
            <Button color="primary" variant="contained" type={'submit'}>
              Sauvegarder
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" onClick={handleClose}>Annuler</Button>
          </Grid>
          <Grid item>
            <Button variant="outlined">Supprimer</Button>
          </Grid>
        </Grid>
      </form>
    </Box>
  </Modal>
}
