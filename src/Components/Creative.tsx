import React from "react";

import {getCreativeByUuid} from "../const";
import {useQuery} from "react-query";

import {
  Button,
  Grid,
  List,
  ListItem,
  ListItemIcon, ListItemText,
  Paper,
  Typography
} from "@mui/material";
import {Person} from "@mui/icons-material";

import {EditCreative} from "./EditCreative";

export const Creative = ({uuid}: any) => {

  const [open, setOpen] = React.useState<boolean>(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const {data} = useQuery(
    ['getCreativeByUuid', uuid],
    () => getCreativeByUuid(uuid)
  )

  const creative = data?.data || []

  const date = new Date(creative?.lastModified);
  const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'} as const;

  return <>
    <Grid item xs={15}>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Typography variant="h6" paragraph>
            {creative.title}
          </Typography>
          <Typography paragraph>{creative.description}</Typography>
          <Typography paragraph>{creative.content}</Typography>
        </Grid>
        <Grid item xs={4}>
          <Paper elevation={0} style={{padding: 16}}>
            <Typography paragraph variant="subtitle2">
              Créé par {creative.createdBy?.firstName} {creative.createdBy?.lastName}
            </Typography>
            <Typography paragraph variant="subtitle2">
              Dernière modification le {date.toLocaleDateString("fr-FR", options)}
            </Typography>
          </Paper>

          <Paper elevation={2}>
            <List>
              {creative.contributors !== undefined && creative.contributors.map((contributor: any) => {
                return <ListItem key={contributor?.id}>
                  <ListItemIcon>
                    <Person/>
                  </ListItemIcon>
                  <ListItemText primary={`${contributor.firstName} ${contributor.lastName}`}/>
                </ListItem>
              })}
            </List>
          </Paper>
        </Grid>
      </Grid>
      <Grid item>
        <Button onClick={handleOpen} variant="outlined">Editer</Button>
      </Grid>
    </Grid>

    <EditCreative uuid={uuid} open={open} handleClose={handleClose}/>
  </>
}
