import React from "react";

import {useQuery} from "react-query";

import {getTotalCreatives} from "../const";

import {Table} from "./Table";
import {Creative} from "./Creative";

import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import {Grid, Pagination} from "@mui/material";

import {modalStyle} from "../style";

function Mockup(): JSX.Element {
  const [page, setPage] = React.useState<number>(1);
  const [creativeUuid, setCreativeUuid] = React.useState<string>('');
  const [open, setOpen] = React.useState<boolean>(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };
  const handleCreativeUuid = (value: string) => {
    setCreativeUuid(value);
    handleOpen()
  };

  const totalCreatives = useQuery(['getTotalCreatives'],
    () => getTotalCreatives())

  const maxPagination: number = totalCreatives.data?.data.length / 10 || 0

  return (
    <Grid container style={{marginTop: 16, marginBottom: 16}} spacing={2}>

      <Grid item xs={2}/>

      <Table page={page} handleCreativeUuid={handleCreativeUuid}/>

      <Grid item xs={12}>
        <Grid container justifyContent="center">
          <Grid item>
            <Pagination count={Math.ceil(maxPagination)} page={page} onChange={handleChange}/>
          </Grid>
        </Grid>
      </Grid>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <Creative uuid={creativeUuid}/>
        </Box>
      </Modal>
    </Grid>
  );
}

export default Mockup;
