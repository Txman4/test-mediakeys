import React from "react";
import { render } from '@testing-library/react';
import {QueryClient, QueryClientProvider} from "react-query";
import {Creative} from "../Creative";

describe('<Creative />', () => {
  it('should render as a base element and be in the document', () => {
    const queryClient = new QueryClient();
    const mockup = render(
      <QueryClientProvider client={queryClient}>
        <Creative />
      </QueryClientProvider>);
    expect(mockup.baseElement).toBeInTheDocument();
  });
});
