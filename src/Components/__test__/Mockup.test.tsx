import React from "react";
import { render } from '@testing-library/react';
import Mockup from "../Mockup";
import {QueryClient, QueryClientProvider} from "react-query";

describe('<Mockup />', () => {
  it('should render as a base element and be in the document', () => {
    const queryClient = new QueryClient();
    const mockup = render(
      <QueryClientProvider client={queryClient}>
        <Mockup />
      </QueryClientProvider>);
    expect(mockup.baseElement).toBeInTheDocument();
  });
});
