import React from "react";
import { render } from '@testing-library/react';
import {QueryClient, QueryClientProvider} from "react-query";
import {Table} from "../Table";

describe('<Table />', () => {
  it('should render as a base element and be in the document', () => {
    const queryClient = new QueryClient();
    const mockup = render(
      <QueryClientProvider client={queryClient}>
        <Table page={0} handleCreativeUuid={'a56293e8-f080-46c6-9508-804767da4de4'} />
      </QueryClientProvider>);
    expect(mockup.baseElement).toBeInTheDocument();
  });
});
