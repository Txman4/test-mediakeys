import React from "react";
import { render } from '@testing-library/react';
import App from './App';
import {QueryClient, QueryClientProvider} from "react-query";

describe('<App />', () => {
  it('should render as a base element and be in the document', () => {
    const queryClient = new QueryClient();
    const app = render(
      <QueryClientProvider client={queryClient}>
        <App />
      </QueryClientProvider>);
    expect(app.baseElement).toBeInTheDocument();
  });
});
