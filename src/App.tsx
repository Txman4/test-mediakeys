import React from "react";
import Mockup from "./Components/Mockup";

function App() {
  return (
    <>
      <header
        style={{
          marginTop: 16,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <img src="mediakeys.png" alt="logo" />
        <Mockup/>
      </header>
    </>
  );
}

export default App;
