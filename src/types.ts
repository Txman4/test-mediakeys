export interface CreativeProps {
  content?: string;
  contributors: Contributors[];
  createdBy: Pick<Contributors, 'lastName' | 'firstName'>;
  description?: string;
  enabled: boolean;
  formats: Formats[];
  id: string;
  title: string;
}

export interface Formats {
  width: number;
  height: number;
}

export interface Contributors {
  lastName: string;
  firstName: string;
  id: string;
}
